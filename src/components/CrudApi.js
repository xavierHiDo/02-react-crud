import React, { useState, useEffect } from "react";
import { helpHttp } from "../helpers/helpHttp";
import CrudForm from "./CrudForm";
import CrudTable from './CrudTable';
import Loader from './Loader';
import Message from './Message';

const CrudApi = () => {
  const [db, setDb] = useState(null);
  const [dataToEdit, setDataToEdit] = useState(null);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  let api = helpHttp();
  let url = "http://localhost:5000/santos";

  useEffect(() => {
    setLoading(true);
    api.get(url).then((res) => {
      // console.log('res', res);
      if(!res.err) {
        setDb(res);
        setError(null);
      } else {
        setDb(null);
        setError(res);
      }
      setLoading(false);
    });
  }, []);

  const createData = (data) => {
    data.id = Date.now();
    let options = {
      body: data,
      headers: {"content-type": "application/json"},
    };
    setLoading(true);
    api.post(url, options).then((res) => {
      console.log(res);
      if(!res.err) {
        setDb([...db, res]);
      } else {
        setError(res);
      }
      setLoading(false);
    });
  };

  const updateData = (data) => {
    let options = {
      body: data,
      headers: {"content-type": "application/json"},
    };
    setLoading(true);
    api.put(`${url}/${data.id}`, options).then((res) => {
      console.log(res);
      if(!res.err) {
        let newData = db.map((el) => (el.id === data.id ? data : el));
        setDb(newData);
      } else {
        setError(res);
      }
      setLoading(false);
    });
  };

  const deleteData = (id) => {
    let isDelete = window.confirm(
      `¿Estás seguro de eliminar el registro con el id '${id}'?`
    );
    if (isDelete) {
      let options = {
        headers: {"content-type": "application/json"},
      };
      setLoading(true);
      api.del(`${url}/${id}`, options).then((res) => {
        console.log(res);
        if(!res.err) {
          let newData = db.filter((el) => el.id !== id);
          setDb(newData);
        } else {
          setError(res);
        }
        setLoading(false);
      });
    } else {
      return;
    }
  };

  return (
    <>
      <h2>CRUD Api</h2>
      <article className="grid-1-2">
        <CrudForm
          createData={createData}
          updateData={updateData}
          dataToEdit={dataToEdit}
          setDataToEdit={setDataToEdit}
        />
        {loading && <Loader />}
        {error && (
          <Message
            msg={`Error ${error.status}: ${error.statusText}`}
            bgColor="#dc3545"
          />
        )}
        {db && (
          <CrudTable
            data={db}
            setDataToEdit={setDataToEdit}
            deleteData={deleteData}
          />
        )}
      </article>
    </>
  );
};

export default CrudApi;